Author of the `icite` package
=============================

Robert Alessi
: Personal email: <alessi@robertalessi.net>
: Institutional email: <robert.alessi@cnrs.fr>
: Affiliation: [UMR 8167 Orient & Méditerranée (Paris, France)](https://www.orient-mediterranee.com)
: More information: <https://ctan.org/pkg/icite> or <https://sr.ht/~ralessi/icite>
