---
author:
- Robert Alessi
title: 'The icite package – README file'
---

Overview
========

`icite` is designed to produce from BibTeX or BibLaTeX bibliographical
databases the different indices of authors and works cited which are
called _indices locorum citatorum_. It relies on a specific
`\icite` command and can operate with either BibTeX or BibLaTeX.

License and disclamer
=====================

icite – Indices locorum citatorum

Copyright ⓒ  2019, 2020, 2021, 2023 Robert Alessi

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Please send error reports and suggestions for improvements to Robert
Alessi:

-   email: <alessi@robertalessi.net>

-   website: <https://sr.ht/~ralessi/icite>

-   comments, feature requests, bug reports:
    <https://todo.sr.ht/~ralessi/icite>

This release of icite consists of the following source files:

-   `icite.dtx`

-   `icite.ins`

-   `Makefile`

Installation
============

1.  Run `'latex icite.ins'` to produce the `icite.sty` file.

2.  To finish the installation you have to move the `icite.sty` file into
    a directory where LaTeX can find it. See the FAQ on `texfaq.org`
    at <https://texfaq.org/FAQ-inst-wlcf> for more on this.

Development, Git Repository
===========================

Browse the code
---------------

You can browse icite repository on the web:
<https://git.sr.ht/~ralessi/icite>

From this page, you can download all the releases of `icite`. For
instructions on how to install `icite`, please see above.

Comments, Feature requests, Bug Reports
---------------------------------------

<https://todo.sr.ht/~ralessi/icite>

Download the repository
-----------------------

`icite` development is facilitated by git, a distributed version
control system. You will need to install git (most Unix/Linux
distributions package it in their repositories).

Use this command to download the repository

    git clone https://git.sr.ht/~ralessi/icite

A new directory named icite will have been created, containing
`icite`.

Git hosting
-----------

Make an account on <https://sr.ht> and navigate (while logged in) to
<https://git.sr.ht/~ralessi/icite>. Click *Clone repo to your account*
and you will have in your account your own repository of `icite` where
you will be able to make whatever changes you like to.

